<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
// include database and object files
include_once '../config/database.php';
include_once '../objects/product.php';
// get database connection
$database = new Database();
$db = $database->getConnection();

// prepare product object
$product = new Product($db);

// set ID property of record to read
 $brandID = isset($_GET['brand_id'] )? $_GET['brand_id'] : die();
 $paging =isset($_GET['paging'] )? $_GET['paging'] : die();
 $number =isset($_GET['number'] )? $_GET['number'] : die();


 $stmt= $product->getListProductBrandPaging($paging,$number,$brandID);
 $num = $stmt->rowCount();


// check if more than 0 record found
 if($num>0)
 {
      // products array
    $products_arr=array();
    $products_arr["records"]=array();

    // retrieve our table contents
    // fetch() is faster than fetchAll()
    // http://stackoverflow.com/questions/2770630/pdofetchall-vs-pdofetch-in-a-loop
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);

        $product_item=array(
            "id" => $ID,
            "name" => $Name,
            "price" => $Price,
            "discount" => $Discount,
            "image_url" => $ImageUrl
        );

        array_push($products_arr["records"], $product_item);
    }

    // set response code - 200 OK
    http_response_code(200);

    // show products data in json format
    echo json_encode($products_arr,JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
}
 else
 {
     // set response code - 404 Not found
     http_response_code(404);

     // tell the user no products found
     echo json_encode(
         array("message" => "No products found.")
     );
 }


?>