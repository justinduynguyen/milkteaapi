<?php
class Product
{
    private $table_name = "product";
    private $conn;
    public $id;
    public $name;
    public $price;
    public $discount;
    public $image_url;


    public function __construct($db){
        $this->conn = $db;
    }

     function getTotalProductInBrand($brandID)
    {

         $query = "Select * from  brand_product bp where bp.BrandID = 1 ";
        // prepare query statement
         $result = $this->conn->prepare($query);
         $result->execute();
         $result = $this->conn->prepare("SELECT FOUND_ROWS()");
         $result->execute();
         $row_count =$result->fetchColumn();
         return $row_count;
    
    }
   public function getListProductBrandPaging($paging,$number,$brandID)
    {

        $total = $this->getTotalProductInBrand($brandID);


        $page_number = ceil($total/$number);
        
        if($paging > $page_number) return null;

        // bind id of record to
        $fist=($paging-1)*$number;
        $last=$number*$paging;
        $query = "Select p.* from  product p ,brand_product bp where bp.ProductID = p.ID and bp.BrandID ={$brandID} limit {$fist},{$last}";

        // prepare query statement
        $stmt = $this->conn->prepare($query);




        // execute query
        $stmt->execute();

        return $stmt;
    }
}
?>