<?php
 class Brand
{
    private $conn;
    private  $table_name = "brand";

    public $id;
    public $name;
    public $image_url;
    public $phone_number;
    public $address;
    public $email;
    public function __construct($db){
        $this->conn = $db;
    }
    function readAll()
    {
        $query = "Select * from " . $this->table_name;
        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // execute query
        $stmt->execute();

        return $stmt;
    }
}
?>