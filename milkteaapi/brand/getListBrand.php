<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// include database and object files
require_once '../config/database.php';
require_once ('../objects/brand.php');


$database = new Database();
$db = $database->getConnection();
//initialize
$brand = new Brand($db);

//query brand
$stmt= $brand->readAll();


$num =$stmt->rowCount();

if($num>0)
{
    // products array
    $brands_arr=array();
    $brands_arr["records"]=array();

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);

        $brand_item=array(
            "id" =>$ID,
            "name" => $Name,
            "ImageUrl" => $ImageUrl,
            "PhoneNumber" => $Phonenumber,
            "Address" => $Address,
            "Email" => $Email
        );


        array_push($brands_arr["records"], $brand_item);
    }
    http_response_code(200);

    // show products data in json format
    echo json_encode($brands_arr,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
}
?>
