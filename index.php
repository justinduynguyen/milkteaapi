﻿<?php
	session_start();
	ob_start(); //cached output cho browser, de su dung ham header
	require_once("lib/connect.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="fontawesome-free-5.1.0-web/css/all.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
<script src="bootstrap/js/jquery.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>

<!-- Library For DATATABLE PLUGGIN -->
<script src="lib/datatable/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css" href="lib/datatable/jquery.dataTables.min.css" />

</head>

<body>
<div id="wrapper">
            
    <!--Menu 2-->
    <nav class="navbar navbar-inverse" style="background-color:#71baab; border-color:#71baab">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span> 
          </button>
          <a class="navbar-brand" href="?mod=brand" style="color:#FFF; font-size:14px"><i class="fas fa-tachometer-alt"></i>&nbsp;BRAND</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
          <ul class="nav navbar-nav navbar-left">          	
            <li><a href="?mod=product" style="color:#FFF"><i class="fas fa-snowflake"></i> PRODUCT</a></li> 
            <li><a href="?mod=user" style="color:#FFF"><i class="fas fa-feather"></i> USER</a></li> 	
            <li><a href="?mod=toppingoption" style="color:#FFF"><i class="fas fa-award"></i> TOPPING OPTION</a></li> 
            <li id="sanpham"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fas fa-th-list"></i> BR <span class="caret"></span></a>
            	<ul class="dropdown-menu">
                	<li><a href="?mod=cus_exp" style="color:#FFF"><i class="fas fa-newspaper"></i> CUS EXP RATING</a></li>
                    <li><a href="?mod=cus_service" style="color:#FFF"><i class="fab fa-hotjar"></i> CUS SERVICE RATING</a></li>
                    <li><a href="?mod=performance" style="color:#FFF"><i class="fab fa-slideshare"></i> PERFORMANCE RATING</a></li>
                    <li><a href="?mod=price_rating" style="color:#FFF"><i class="fas fa-clipboard-list"></i> PRICE RATING</a></li>   					<li><a href="?mod=quality_rating" style="color:#FFF"><i class="fas fa-clipboard-list"></i> QUALITY RATING</a></li>   
                </ul>
            </li>   
            <li id="sanpham"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fas fa-th-list"></i> PP <span class="caret"></span></a>
            	<ul class="dropdown-menu">
                	<li><a href="?mod=ice_per" style="color:#FFF"><i class="fas fa-newspaper"></i> ICE PERCENT</a></li>
                    <li><a href="?mod=size" style="color:#FFF"><i class="fab fa-hotjar"></i> SIZE</a></li>
                    <li><a href="?mod=sugar_per" style="color:#FFF"><i class="fab fa-slideshare"></i> SUGAR PERCENT</a></li>
                </ul>
            </li>              
          </ul>
        </div>
      </div>
    </nav>   
    
    <!-- Body -->
    <div class="container" style="padding-bottom:70px">
    	<?php $mod=@$_GET['mod'];
			  if($mod=='') $mod='brand';
			  include("module/back/{$mod}.php");
		?>
    </div> 

    <!-- Footer -->
    <div class="container-fluid" style="background-color:#71baab; color:#FFF; height:50px; text-align:center; width:100%; overflow:hidden; bottom:0; position:absolute;">
    	<span style="line-height:50px;">&copy; Copyright by DuyTran IT - Emmanuel</span>
    </div>

</div>
</body>

	<title>
		<?php   
				if($mod=="home")echo"Trang Chủ";
				if($mod=="login")echo"Đăng Nhập";			
        ?>	
    </title>  
			
</html>