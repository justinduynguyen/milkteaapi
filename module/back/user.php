<div style="padding-bottom:400px">
<table width="506" border="1" style="text-align:center" class="col-md-12 col-sm-12 col-xs-12">
  <caption style="text-align:center; margin-bottom:20px">
    <h2><i class="fas fa-clipboard-check"></i> USER LIST</h2>
  </caption>
  <tr>
    <td width="56" height="31" align="center" bgcolor="#FFFFCC"><h4><strong>Index</strong></h4></td>
    <td width="100" align="center" bgcolor="#FFFFCC"><h4><strong>Full Name</strong></h4></td>
    <td width="100" align="center" bgcolor="#FFFFCC"><h4><strong>Username</strong></h4></td>
    <td width="73" align="center" bgcolor="#FFFFCC"><h4><strong>Image</strong></h4></td>  
    <td width="73" align="center" bgcolor="#FFFFCC"><h4><strong>Phone Number</strong></h4></td>
    <td width="90" align="center" bgcolor="#FFFFCC"><h4><strong>Address</strong></h4></td>            
    <td width="120" align="center" bgcolor="#FFFF99"><h4><strong><a href="?mod=user_add"><i class="fas fa-plus-circle"></i> Add</a></strong></h4></td>
  </tr>
  <?php   	
	$sql="select * from `user`";
	$rs=mysqli_query($link,$sql);  
	$i=1;
	while($r=mysqli_fetch_assoc($rs)) {
  ?>
  <tr class="nen">
    <td height="37" align="center"><h5>
      <?=$i++?>
    </h5></td>
    <td><h5>
      <?=$r['Fullname']?>
    </h5></td>
    <td><h5>
      <?=$r['Username']?>
    </h5></td>
    <td align="center"><h5>
	  <img src="<?=$r['ImageUrl']?>" style="width:50px; height:50px">       
    </h5></td>
    <td align="center"><h5>
      <?=$r['Phonenumber']?>
    </h5></td>
    <td align="center"><h5>
      <?=$r['Address']?>
    </h5></td>
    <td align="center"><h5><a href="?mod=user_del&id=<?=$r['ID']?>" onClick="return confirm('Chắc chắn xóa?')">Delete</a></h5></td>
  </tr>
  <?php } ?>
</table>
</div>