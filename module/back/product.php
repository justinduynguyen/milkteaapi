<div style="padding-bottom:400px">
<table width="506" border="1" style="text-align:center" class="col-md-12 col-sm-12 col-xs-12">
  <caption style="text-align:center; margin-bottom:20px">
    <h2><i class="fas fa-clipboard-check"></i> PRODUCT LIST</h2>
  </caption>
  <tr>
    <td width="56" height="31" align="center" bgcolor="#FFFFCC"><h4><strong>Index</strong></h4></td>
    <td width="145" align="center" bgcolor="#FFFFCC"><h4><strong>Name</strong></h4></td>
    <td width="100" align="center" bgcolor="#FFFFCC"><h4><strong>Image</strong></h4></td>
    <td width="73" align="center" bgcolor="#FFFFCC"><h4><strong>Price</strong></h4></td>  
    <td width="73" align="center" bgcolor="#FFFFCC"><h4><strong>Discount</strong></h4></td>        
    <td width="98" align="center" bgcolor="#FFFF99"><h4><strong><a href="?mod=product_add"><i class="fas fa-plus-circle"></i> Add</a></strong></h4></td>
  </tr>
  <?php   	
	$sql="select * from `product`";
	$rs=mysqli_query($link,$sql);  
	$i=1;
	while($r=mysqli_fetch_assoc($rs)) {
  ?>
  <tr class="nen">
    <td height="37" align="center"><h5>
      <?=$i++?>
    </h5></td>
    <td><h5>
      <?=$r['Name']?>
    </h5></td>
    <td align="center"><h5>
	  <img src="<?=$r['ImageUrl']?>" style="width:50px; height:50px">       
    </h5></td>
    <td align="center"><h5>
      <?=number_format($r['Price'])?> <strong>VND</strong>
    </h5></td>
    <td align="center"><h5>
      <?=$r['Discount']?>%
    </h5></td>
    <td align="center"><h5><a href="?mod=product_upd&id=<?=$r['ID']?>">Update</a> | <a href="?mod=product_del&id=<?=$r['ID']?>" onClick="return confirm('Chắc chắn xóa?')">Delete</a></h5></td>
  </tr>
  <?php } ?>
</table>
</div>