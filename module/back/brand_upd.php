<?php
	if(isset($_GET['id']))
	{
		$id=$_GET['id'];
		
		$sql="select * from `brand` where `ID`={$id}";
		$rs=mysqli_query($link,$sql);
		$r=mysqli_fetch_assoc($rs);
	}
		
	if(isset($_POST['name']))
	{
		$name=$_POST['name'];
		$mobile=$_POST['mobile'];
		$address=$_POST['address'];
		$email=$_POST['email'];		
		
		//Xử lý file
		$file = $_FILES['img_url'];
		
		if($file['name']!='')//Có submit file
		{
			//Lay ten file			
			$img_url = mt_rand().$file['name'];//mt_rand(): sinh so ngau nhien, xu ly trung ten file
			//Copy file toi thu muc chua anh
			copy($file['tmp_name'],"img/brand/{$img_url}");
			$img_url = "img/brand/".$img_url;
		
			$sql="UPDATE `brand` SET `Name`='{$name}', `ImageUrl`='{$img_url}',`Phonenumber`='{$mobile}',`Address`='{$address}', `Email`='{$email}' WHERE `ID`={$id}";
			mysqli_query($link, $sql);
			
			//Xóa hình cũ sau khi cập nhật hình mới
			$hinhcu="{$r['ImageUrl']}";
			unlink($hinhcu);		
		}
		else
		{
			$sql="UPDATE `brand` SET `Name`='{$name}', `Phonenumber`='{$mobile}',`Address`='{$address}', `Email`='{$email}' WHERE `ID`={$id}";
			mysqli_query($link, $sql);
		}
		
		//Chuyen den trang view
		header('location:?mod=brand');
	}
?>

<div class="container">
<div class="row">
<div class="col-md-3 col-sm-3 col-xs-12"></div> 

<?php
	$sql="select * from `brand` where `ID`={$id}";
	$kq=mysqli_query($link,$sql);
	$s_kq=mysqli_fetch_assoc($kq);
?>
<form action="" method="post" enctype="multipart/form-data">
<table width="421" height="171" border="1"  class="col-md-6 col-sm-6 col-xs-12">
	<caption style="text-align:center">
    	<h2>UPDATE BRAND</h2>
  	</caption>
  
  <tr>
    <td width="108" height="36" align="center">Name <span style="color:#F00">*</span></td>
    <td width="297" align="left">&nbsp;
    	<input type="text" name="name" value="<?=$r['Name']?>" required>
    </td>
  </tr>
  <tr>
    <td width="108" height="36" align="center">Email <span style="color:#F00">*</span></td>
    <td width="297" align="left">&nbsp;
    	<input type="email" name="email" value="<?=$r['Email']?>">
    </td>
  </tr>
  <tr>
    <td height="157" align="center">Image</td>
    <td align="left" style="line-height:10px">&nbsp;
      <img src="<?=$s_kq['ImageUrl']?>" alt="<?=$s_kq['Name']?>" height="100px"><br><br>
      <input type="file" name="img_url" id="fileField" style="margin-left:8px"><br>
      <em> (Để trống nếu không muốn cập nhật hình) </em>
    </td>
  </tr>
  <tr>
    <td height="36" align="center">PhoneNumber <span style="color:#F00">*</span></td>
    <td align="left">&nbsp;
      <input type="number" name="mobile" value="<?=$s_kq['Phonenumber']?>" required style="margin-left:8px"></td>
  </tr>
  <tr>
    <td height="37" align="center">Address</td>
    <td align="left">&nbsp;&nbsp;
      <label for="textarea"></label>
      <textarea name="address" cols="45" rows="5"><?=$s_kq['Address']?></textarea>
      <label for="textfield"></label></td>
  </tr>
  <tr align="center">
    <td height="51" colspan="2">
      <input type="submit" value="Update Brand"  class="btn btn-success">&nbsp;&nbsp;&nbsp;
      <input type="reset" value="Reset"  class="btn btn-success">
    </td>
  </tr>
 
</table>
</form>

<div class="col-md-3 col-sm-3 col-xs-12"></div> 
</div>
</div>