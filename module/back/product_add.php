<?php
	if(isset($_POST['name']))
	{
		$name=$_POST['name'];
		$price=$_POST['price'];		
		$discount=$_POST['discount'];
		
		//Xử lý file
		$file = $_FILES['img_url'];
		if($file['name']!='')//Có submit file
		{
			//Lay ten file			
			$img_url = mt_rand().$file['name'];//mt_rand(): sinh so ngau nhien, xu ly trung ten file
			//Copy file toi thu muc chua anh
			copy($file['tmp_name'],"img/product/{$img_url}");
			$img_url = "img/product/".$img_url;
		}
		
		$sql="INSERT INTO `product` values (NULL,'{$name}','{$price}','{$discount}','{$img_url}')";
		mysqli_query($link,$sql);
		
		//Chuyen den trang view
		header('location:?mod=product');		
	}
?>

<div class="container">
<div class="row">
<div class="col-md-3 col-sm-3 col-xs-12"></div> 

<form action="" method="post" enctype="multipart/form-data">
<table width="421" height="171" border="1"  class="col-md-6 col-sm-6 col-xs-12">
	<caption style="text-align:center">
    	<h2>ADD PRODUCT</h2>
  	</caption>
  
  <tr>
    <td width="108" height="36" align="center">Name <span style="color:#F00">*</span></td>
    <td width="297" align="left">&nbsp;
    	<input type="text" name="name" required>
    </td>
  </tr>
  <tr>
    <td height="36" align="center">Image <span style="color:#F00">*</span></td>
    <td align="left" style="line-height:10px">&nbsp;
      <label for="fileField"></label>
      <input type="file" name="img_url" id="fileField" style="padding-left:9px"></td>
  </tr>
  <tr>
    <td height="37" align="center">Price <span style="color:#F00">*</span></td>
    <td align="left">&nbsp;&nbsp;<input type="number" name="price" min="1" required></td>
  </tr>
  <tr>
    <td height="37" align="center">Discounts</td>
    <td align="left">&nbsp;&nbsp;<input type="number" name="discount" min="0"></td>
  </tr>
  <tr align="center">
    <td height="51" colspan="2">
      <input type="submit" value="Add Product"  class="btn btn-success">&nbsp;&nbsp;&nbsp;
      <input type="reset" value="Reset"  class="btn btn-success">
    </td>
  </tr>
 
</table>
</form>

<div class="col-md-3 col-sm-3 col-xs-12"></div> 
</div>
</div>