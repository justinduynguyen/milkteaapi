<?php
	if(isset($_POST['name']))
	{
		$name=$_POST['name'];
		$email=$_POST['email'];		
		$mobile=$_POST['mobile'];
		$address=$_POST['address'];
		
		//Xử lý file
		$file = $_FILES['img_url'];
		
		if($file['name']!='')//Có submit file
		{
			//Lay ten file			
			$img_url = mt_rand().$file['name'];//mt_rand(): sinh so ngau nhien, xu ly trung ten file
			//Copy file toi thu muc chua anh
			copy($file['tmp_name'],"img/brand/{$img_url}");
			$img_url = "img/brand/".$img_url;
		}
		
		$sql="insert into `brand` values (NULL,'{$name}','{$img_url}','{$mobile}','{$address}','{$email}')";
		mysqli_query($link,$sql);
		
		//Chuyen den trang view
		header('location:?mod=brand');		
	}
?>

<div class="container">
<div class="row">
<div class="col-md-3 col-sm-3 col-xs-12"></div> 

<form action="" method="post" enctype="multipart/form-data">
<table width="421" height="171" border="1"  class="col-md-6 col-sm-6 col-xs-12">
	<caption style="text-align:center">
    	<h2>ADD BRAND</h2>
  	</caption>
  
  <tr>
    <td width="108" height="36" align="center">Name <span style="color:#F00">*</span></td>
    <td width="297" align="left">&nbsp;
    	<input type="text" name="name" required>
    </td>
  </tr>
  <tr>
    <td width="108" height="36" align="center">Email</td>
    <td width="297" align="left">&nbsp;
    	<input type="email" name="email">
    </td>
  </tr>
  <tr>
    <td height="36" align="center">Image <span style="color:#F00">*</span></td>
    <td align="left" style="line-height:10px">&nbsp;
      <label for="fileField"></label>
      <input type="file" name="img_url" id="fileField" style="padding-left:9px"></td>
  </tr>
  <tr>
    <td height="37" align="center">PhoneNumber <span style="color:#F00">*</span></td>
    <td align="left">&nbsp;&nbsp;<input type="number" name="mobile" required></td>
  </tr>
  <tr>
    <td height="37" align="center">Address <span style="color:#F00">*</span></td>
    <td align="left">&nbsp;&nbsp;<textarea style="width:400px; height:80px" name="address" required></textarea></td>
  </tr>
  <tr align="center">
    <td height="51" colspan="2">
      <input type="submit" value="Add Brand"  class="btn btn-success">&nbsp;&nbsp;&nbsp;
      <input type="reset" value="Reset"  class="btn btn-success">
    </td>
  </tr>
 
</table>
</form>

<div class="col-md-3 col-sm-3 col-xs-12"></div> 
</div>
</div>